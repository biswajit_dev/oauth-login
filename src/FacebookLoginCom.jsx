import React from "react";
import FacebookLogin from "react-facebook-login";

const FacebookLoginCom = () => {
  const responseFacebook = (response) => {
    console.log(response);
  };
  return (
    <div>
      <FacebookLogin
        appId={process.env.REACT_APP_FACEBOOK_APP_ID}
        autoLoad={true}
        fields="name,email,picture"
        callback={responseFacebook}
      />
    </div>
  );
};

export default FacebookLoginCom;
