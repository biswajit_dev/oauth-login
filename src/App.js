import React, { useState } from "react";
import GoogleLoginCom from "./GoogleLoginCom";
import FacebookLoginCom from "./FacebookLoginCom";

const App = () => {
  const [toShow, setToshow] = useState("google");

  return (
    <div>
      <div style={{ display: "flex", marginBottom: "4rem" }}>
        <input
          type="radio"
          name="google"
          value={toShow}
          checked={toShow === "google"}
          onChange={() => setToshow("google")}
          style={{ cursor: "pointer" }}
        />
        <label style={{ marginRight: "2rem" }}>Google Login</label>
        <input
          type="radio"
          name="facebook"
          value={toShow}
          checked={toShow === "facebook"}
          onChange={() => setToshow("facebook")}
          style={{ cursor: "pointer" }}
        />
        <label style={{ marginRight: "2rem" }}>Facebook Login</label>
      </div>
      {toShow === "google" ? <GoogleLoginCom /> : <FacebookLoginCom />}
    </div>
  );
};

export default App;
